# AED-Guia1
# Guia1-UI
Actividades primera guia de AED 2019

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta del ejercicio correspondiente (por ejemplo, "ej1") y ejecutar por terminal:
```
g++ programa.cpp Clase_correspondiente.cpp -o programa
make
./programa
```

# Actividades
1. ej1

	El departamento de personal de la Universidad tiene registros del nombre, sexo y edad de cada uno de los profesores (leer esta información). Escriba un programa que calcule e imprima los siguientes datos:

	* Edad promedio del grupo de profesores

	* Nombre del profesor más joven

	* Nombre del profesor de más edad

	* Número de profesoras con edad mayor al promedio

	* Número de profesores con edad menor al promedio

2. ej2

	Al momento de su ingreso al hospital, a un paciente se le solicitan los siguientes datos: Nombre, Edad, Sexo, Domicilio (Calle, número, ciudad), Teléfono, Seguro médico (verdadero o falso). Escriba un programa que pueda llevar a cabo las siguientes operaciones:

	* Listar los nombres de todos los pacientes hospitalizados.

	* Obtener el porcentaje de pacientes hospitalizados en las siguientes categorı́as (dadas por la edad):

		- Niños: hasta 13 años.

		- Jóvenes: mayores de 13 años y menores de 30.

		- Adultos: mayores de 30 años.

	* Obtener el porcentaje de hombres y de mujeres hospitalizadas.

	* Dado el nombre del paciente, listar todos los datos relacionados con dicho paciente.

	* Calcular el porcentaje de pacientes que poseen seguro médico.

3. ej3

	Una inmobiliaria tiene información sobre departamentos en arriendo. De cada departamento se conoce:

	* Clave: es un entero que identifica al inmueble.

	* Extensión: superficie del depto en metros cuadrados.

	* Ubicación: (excelente, buena, regular, mala).

	* Precio: es un entero

	* Disponible: verdadero o falso

	Diariamente acuden muchos clientes a la inmobiliaria solicitando información. Escriba un programa capaz de realizar las siguientes operaciones sobre la información disponible:

	* Liste los datos de todos los deptos disponibles que tengan un precio inferior o igual a cierto valor P.

	* Liste los datos de los deptos disponibles que tengan una superficie mayor o igual a un cierto valor dado E y una ubicación excelente.

	* Liste el monto del arriendo de todos los deptos arrendados.

	* Llega un cliente solicitando arrendar un depto. Si existe alguno con una superficie mayor o igual a la deseada, con precio y ubicación que se ajustan a las necesidades del cliente, el depto de le arrendará.

	* Se ha decidido aumentar el arriendo en un X %. Actualizar los precios de los arriendos de los deptos no alquilados.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela

