# Ejercicio1 Guia1-UI

Programa que entrega como resultado un resumen de la informacion ingresada sobre los docentes.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta "ej1" y ejecutar por terminal:
```
g++ programa.cpp Profesor.cpp -o programa
make
./programa
```

# Acerca de

El inicio del programa se solicita ingresar la cantidad de docentes que queremos añadir.
A continuación, se solicita ingresar los datos (con los que se llena el arreglo) para finalmente mostrar el resultado.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
