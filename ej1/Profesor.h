#include <iostream>
using namespace std;

#ifndef PROFESOR_H
#define PROFESOR_H

class Profesor {
	private:
		//int max;
		//*profesor;
		string nombre;
		string sexo;
		int edad;
		//float promedio;

	public:
		/* constructor */
		Profesor();
		Profesor(string nombre, string sexo, int edad);

		/* métodos get and set */
		string get_nombre();
		string get_sexo();
		int get_edad();
		//float get_promedio();

		void set_nombre(string nombre);
		void set_sexo(string sexo);
		void set_edad(int edad);
		//void operacion_promedio(int *profesor, int max);
};
#endif