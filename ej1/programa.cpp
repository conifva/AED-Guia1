#include <iostream>
#include <stdio.h>
#include "Profesor.h"

using namespace std;

void imprimir_datos(int max, Profesor *profesor, float edad_promedio, int menor_prom, int mayor_prom){
		/* Mostrar datos*/
		cout << "\n\n\tDATOS " << endl;
		cout << "  + Edad promedio de grupo de profesores: " << edad_promedio << endl;
		cout << "  + Número de profesoras con edad mayor al promedio: " << mayor_prom << endl;
		cout << "  + Número de profesores con edad menor al promedio: " << menor_prom << endl;
		//continua en joven_anciano
}

void joven_anciano(int max, Profesor *profesor){
	/* Ordena el arreglo */
	Profesor aux; 
	for(int j=1;j<=max;j++){
		for(int i=0;i<max-1;i++){
			if(profesor[i].get_edad()>profesor[i+1].get_edad()){
				aux=profesor[i];
				profesor[i]=profesor[i+1];
				profesor[i+1]=aux;
			}
		}
	}

	/* resultado */
	cout << "  + Nombre del profesor más joven: " << profesor[0].get_nombre() << endl;
	cout << "  + Nombre del profesor de más edad: "  << profesor[max-1].get_nombre() <<endl;
	
}

int cant_menores(int max, float edad_promedio, Profesor *profesor){
	int menores=0; // contador
	for (int i = 0; i < max; i++){
		/* si encuentra una edad menor suma 1 al contador*/
		if (profesor[i].get_edad()  < edad_promedio){
			menores = menores + 1;
		}
		/* sino se mantiene */
		else{
			menores = menores;
		}
	}
	return menores;
}

int cant_mayores(int max, float edad_promedio, Profesor *profesor){
	/* mismo sistema que con cant_menores */
	int mayores=0; // contador
	for (int i = 0; i < max; i++){
		/* si encuentra una edad menor suma 1 al contador*/
		if (profesor[i].get_edad()  > edad_promedio)
		{
			mayores = mayores + 1;
		}
		/* sino se mantiene */
		else{
			mayores = mayores;
		}
	}
	return mayores;
}

float calculo_promedio(int max, Profesor *profesor){
	float suma=0; //contador
	for (int i = 0; i < max; ++i)
	{
		/* suma las edades */
		suma += profesor[i].get_edad();
	}
	/* entrega el promedio*/
	return (suma/max);
}

void llenar_datos(int max, Profesor *profesor){
	string nombre;
	string sexo;
	int edad;

	for (int i = 0; i < max; i++){
		/* ingreso de datos */
		cout << "\n\tPROFESOR [" << i << "]" << endl;
		cout << "Nombre: ";
		cin >> nombre;
		cout << "Sexo: ";
		cin >> sexo;
		cout << "Edad: ";
		cin >> edad;
		
		/* agrega datos */
		profesor[i].set_nombre(nombre);
		profesor[i].set_sexo(sexo);
		profesor[i].set_edad(edad);
	}

}

int main()
{
	int max;
	float edad_promedio=0;
	int menor_prom=0;
	int mayor_prom=0;

	cout << "\n\tREGISTRO PROFESORES";
	cout << "\n>Ingrese la cantidad de clientes: ";
	cin >> max;

	/* arreglo de tipo Profesor */
	Profesor profesor[max];
	/* pasos: 
	 *  - llenar datos arreglo
	 *  - hacer calculos, etc
	 *  - mostrar por terminal
	 */
	llenar_datos(max, profesor);
	edad_promedio=calculo_promedio(max, profesor);
	menor_prom=cant_menores(max, edad_promedio, profesor);
	mayor_prom=cant_mayores(max, edad_promedio, profesor);
	imprimir_datos(max, profesor, edad_promedio, menor_prom, mayor_prom);
	joven_anciano(max, profesor);

	return 0;
}