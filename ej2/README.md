# Ejercicio1 Guia1-UI

Programa que entrega como resultado la suma del cuadrado de los números ingresados.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta "ej2" y ejecutar por terminal:
```
g++ programa.cpp Paciente.cpp -o programa
make
./programa
```

# Acerca de

///POR EDITAAAR
El inicio del programa se solicita ingresar la cantidad de numeros sobre los que se trabajará, este número correspondera al largo del arreglo.
A continuación, se solicita ingresar los datos (con los que se llena el arreglo) para finalmente mostrar el resultado.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela
